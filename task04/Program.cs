﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task04
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a decimal number");
            doubles(Console.ReadLine());
        }
        static void doubles(string a)
        {
            double input = double.Parse(a);
            Console.WriteLine($"{input} + {input}");
            Console.WriteLine($"{input} - {input}");
            Console.WriteLine($"{input} / {input}");
            Console.WriteLine($"{input} * {input}");

        }
    }
}
